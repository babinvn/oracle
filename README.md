# Using Oracle database docker container with Hetzner

```
# docker-machine rm oracle
docker-machine create \
--driver=hetzner \
--hetzner-api-token=$HETZNER_TOKEN \
--hetzner-server-type=cx31 \
--hetzner-server-location=fsn1 \
oracle
```

Do not open Oracle database ports to the Internet, use local networking.
Add the server to your Hetzner network (`network-1`) and setup docker networking as follows:

```
docker-machine ssh oracle

docker network create -d bridge \
-o com.docker.network.bridge.host_binding_ipv4=$(ifconfig eth0 | grep "inet " | awk '{print $2}') \
internet

docker network create -d bridge \
-o com.docker.network.bridge.host_binding_ipv4=$(ifconfig ens10 | grep "inet " | awk '{print $2}') \
intranet
```

Use separate volume to store the database data files. You can later use this volume
with another docker machine (or in case you need to recreate your Oracle server). Create
and connect `oracle` volume and make sure it is mounted as `/var/docker/oracle`.
Edit the docker machine's `/etc/fstab` file if necessary.

Create two subdirectories and grant them to `oracle` user:

```
mkdir -p /var/docker/oracle/oradata /var/docker/oracle/diag
chown -R 54321:54321 /var/docker/oracle/oradata /var/docker/oracle/diag
```

Pull the image. You can only use your private repository to store Oracle images.
For details on building an Oracle database image please refer to https://bitbucket.org/babinvn/docker-oracle

```
docker login my.private.registry
docker pull my.private.registry/oracle-database:19.3.0-se2
```

Then you may run docker-compose.

To connect to the database:

```
docker exec -u oracle -it oracle sqlplus sys@//127.0.0.1:1521/jiradb as sysdba
docker exec -u oracle -it oracle sqlplus system@//127.0.0.1:1521/jiradb
...
```
